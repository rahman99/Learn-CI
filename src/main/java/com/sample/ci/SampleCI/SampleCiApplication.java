package com.sample.ci.SampleCI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleCiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleCiApplication.class, args);
	}
}

package com.sample.ci.SampleCI;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SampleCIController {
	
	@RequestMapping("/")
	public String index(){
		return greeting();
	}

	@RequestMapping("/index")
    public String greeting() {
        return "welcome in index";
    }
	
	@RequestMapping("/test")
	public String test(){
		return "running test";
	}
}
